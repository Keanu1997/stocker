@extends('layouts.app')
@section('title', 'Create Product')

@section('content')
    <div class="row">
        <div class="col">
            <h2>Create a Product</h2>
        </div>
    </div>
    <div class="row">
        <div class="col">
            <div id="alertCreateProduct" role="alert"></div>
            <form id="createProductForm" action="{{ route('product.store') }}" method="POST">
                <meta name="csrf-token" content="{{ csrf_token() }}">
                <div class="mb-3">
                    <label for="name" class="form-label">Name:</label>
                    <input class="form-control" type="text" name="name" id="name">
                </div>
                <div class="mb-3">
                    <label for="sku" class="form-label">Sku:</label>
                    <input class="form-control" type="text" name="sku" id="sku">
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description:</label>
                    <input class="form-control" type="text" name="description" id="description">
                </div>
                <div class="mb-3">
                    <label for="stock" class="form-label">Stock:</label>
                    <input class="form-control" type="number" name="stock" id="stock">
                </div>
                <div class="mb-3">
                   <div class="form-check">
                       <input type="checkbox" class="form-check-input" id="hidden">
                       <label for="hidden" class="form-check-label">
                            Hidden
                       </label>
                   </div>
                </div>
                <div class="mb-3">
                    <button id="createProductBtn" class="btn btn-success">Create product</button>
                </div>
            </form>
        </div>
    </div>
@endsection

@push('foot')
<script>
    document.getElementById("createProductForm").addEventListener('submit', handleCreateProduct);

    async function handleCreateProduct(event) {
        event.preventDefault();

        const token = document.querySelector('meta[name="csrf-token"]').content;
        const form = event.currentTarget;
        const url = form.action;
        const formData = new FormData(form);

        let alert = document.getElementById('alertCreateProduct');
        
        fetch(url, {
            headers: {
                "Accept": "application/json",
                "X-Requested-With": "XMLHttpRequest",
                "X-CSRF-TOKEN": token
            },
            method: 'POST',
            credentials: "same-origin",
            body: formData
        })
        .then((response) => response
        .json()
            .then((json) => ({
            ok: response.ok,
            status: response.status,
            body: json,
            }))
        )
        .then(({ ok, status, body }) => {
            if (!ok) {
                const message = getFormValidationMessage(status, body);
                throw new Error(message);
            }

            alert.className = 'alert alert-success';
            alert.innerHTML = body.message;
        })
        .catch(function(error) {
            alert.className = 'alert alert-danger';
            alert.innerHTML = error;
        });

        const getFormValidationMessage = (status, body) => {
            if (status !== 422) return "Something went wrong"; // Guard clause, Laravel throws a 422. Just set a default message
                let errorString = "";

            for (const [_, message] of Object.entries(body.errors)) {
                errorString += `<br />${message}`;
            }

            return errorString; // Make a pretty message in the format below
        };
    }
    
</script>
@endpush