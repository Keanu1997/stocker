@extends('layouts.app')
@section('title', 'Product Details')

@section('content')
<div class="row">
    <div class="col">
        <h2>Product Details</h2>
    </div>
</div>
<div class="row">
    <div class="col-md-auto">
        <div class="card" style="width: 18rem;">
            <div class="card-body">
                <h5 class="card-title">{{ $product->name }}</h5>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Sku: {{ $product->sku }}</li>
                <li class="list-group-item">Stock: {{ $product->stock }}</li>
                <li class="list-group-item">Hidden: {{ $product->hidden === 0 ? 'no' : 'yes' }}</li>
            </ul>
            <div class="card-body">
                <a href="#" class="btn btn-warning">Update</a>
                <a href="#" class="btn btn-danger">Delete</a>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-header">Description:</div>
            <div class="card-body">
                <p>{{ $product->description }}</p>
            </div>
        </div>
    </div>
</div>
@endsection