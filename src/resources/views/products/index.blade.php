@extends('layouts.app')
@section('title', 'Product Overview')

@section('content')
<div class="row">
    <div class="col">
        <h2>Product List</h2>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="table-responsive-xl">
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">SKU</th>
                        <th scope="col">Stock</th>
                        <th scope="col">Hidden</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($products as $product)
                        <tr scope="row">
                            <th scope="row">{{ $product->id }}</th>
                            <td>{{ $product->name }}</td>
                            <td>{{ $product->sku }}</td>
                            <td>{{ $product->stock }}</td>
                            <td>{{ $product->hidden === 0 ? 'no' : 'yes' }}</td>
                            <td>
                                <div class="row">
                                    <div class="col">
                                        <a class="btn btn-primary" href="{{ route('product.show', $product->id) }}" role="button">Details</a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $products->links() }}
        </div>
    </div>
</div>
@endsection