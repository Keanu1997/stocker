<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;


class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'          => $this->faker->sentence(5),
            'sku'           => $this->faker->ean13(),
            'description'   => $this->faker->text(255),
            'stock'         => $this->faker->numberBetween(0, 9999),
            'hidden'        => $this->faker->boolean(50)
        ];
    }
}
