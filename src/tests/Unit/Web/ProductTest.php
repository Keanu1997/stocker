<?php

namespace Tests\Unit\Web;

use App\Models\Product;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ProductTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @test
     * @return void
     */
    public function it_can_create_a_product(): void
    {
        $product = Product::factory()->create();

        $this->assertDatabaseHas('products', ['id'  => $product->id]);
    }
}
