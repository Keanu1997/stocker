<?php

namespace Tests\Feature\Web;

use App\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @test
     * @return void
     */
    public function it_can_show_a_create_page(): void
    {
        // When
        $response = $this->get('/product/create');

        // Then
        $response->assertOk();
        $response->assertSee('Name:');
        $response->assertSee('Sku:');
        $response->assertSee('Description:');
        $response->assertSee('Stock:');
        $response->assertSee('Hidden');
    }

    /**
     * @test
     * @return void
     */
    public function it_can_store_a_product(): void
    {
        // Given
        $product = [
            'name'          => $this->faker->sentence(5),
            'sku'           => $this->faker->ean13,
            'description'   => $this->faker->paragraph,
            'stock'         => $this->faker->numberBetween(0, 9999),
            'hidden'        => $this->faker->boolean(50)   
        ];

        // When
        $response = $this->post('/product', $product);

        // Then
        $response->assertStatus(201);
        $response->assertJson([
            'message'   => 'Created'
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function it_returns_an_error_when_sku_not_unique(): void
    {
        // Given
        $product1 = [
            'name'          => $this->faker->sentence(5),
            'sku'           => 'sku-123',
            'description'   => $this->faker->paragraph,
            'stock'         => $this->faker->numberBetween(0, 9999),
            'hidden'        => $this->faker->boolean(50)   
        ];

        $product2 = [
            'name'          => $this->faker->sentence(5),
            'sku'           => 'sku-123',
            'description'   => $this->faker->paragraph,
            'stock'         => $this->faker->numberBetween(0, 9999),
            'hidden'        => $this->faker->boolean(50)   
        ];

        $this->post('/product', $product1);

        // When
        $response = $this->post('/product', $product2);

        // Then
        $response->assertStatus(422);
        $response->assertJsonFragment([
            0 => 'The sku has already been taken.'
        ]);
    }

    /**
     * @test
     * @return void
     */
    public function it_can_list_all_on_page_one(): void
    {
        // Given
        $products = Product::factory()->count(50)->create();
        $pageNum = 1;
        $nextPageNum = 2;
        $pageProducts = $products->forPage($pageNum, 10);
        $nextPageProducts = $products->forPage($nextPageNum, 10);

        // When
        $response = $this->get("/product?page=$pageNum");

        // Then
        $response->assertOk();
        $response->assertViewIs('products.index');
        
        foreach($pageProducts as $product)
        {
            $response->assertSee($product->sku);
        }

        foreach($nextPageProducts as $product)
        {
            $response->assertDontSee($product->sku);
        }
    }

    /**
     * @test
     * @return void
     */
    public function it_can_list_all_on_page_two(): void
    {
        // Given
        $products = Product::factory()->count(50)->create();
        $pageNum = 2;
        $nextPageNum = 3;
        $pageProducts = $products->forPage($pageNum, 10);
        $nextPageProducts = $products->forPage($nextPageNum, 10);

        // When
        $response = $this->get("/product?page=$pageNum");

        // Then
        $response->assertOk();
        $response->assertViewIs('products.index');
        
        foreach($pageProducts as $product)
        {
            $response->assertSee($product->sku);
        }

        foreach($nextPageProducts as $product)
        {
            $response->assertDontSee($product->sku);
        }
    }

    /**
     * @test
     * @return void
     */
    public function it_can_show_details(): void
    {
        // Given
        $products = Product::factory()->count(2)->create();
        $product = $products->first();

        // When
        $response = $this->get('/product/' . $product->id);

        // Then
        $response->assertOk();
        $response->assertViewIs('products.show');
        $response->assertViewHasAll(['product' => $product]);
        $response->assertSee('Sku: ' . $product->sku);
        $response->assertSee('Stock: ' . $product->stock);
        $response->assertSee('Hidden: ' . $product->stock === 0 ? 'no' : 'yes');
    }
}
