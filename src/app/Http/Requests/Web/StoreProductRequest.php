<?php

namespace App\Http\Requests\Web;

use App\Http\Requests\FormRequest;

class StoreProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'          => 'required|string|min:3|max:255',
            'sku'           => 'required|string|min:3|max:255|unique:App\Models\Product',
            'description'   => 'required|string|min:3|max:255',
            'stock'         => 'required|integer',
            'hidden'        => 'boolean'
        ];
    }
}
