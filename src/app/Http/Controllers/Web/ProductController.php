<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Requests\Web\StoreProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\View\View;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $products = Product::paginate(10);
        $products->withPath('/product');

        return view('products.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('products.create');
    }

   /**
     * Store a newly created resource in storage.
     *
     * @param StoreProductRequest $request
     * @return JsonResponse
     */
    public function store(StoreProductRequest $request): JsonResponse
    {
        $validated = $request->validated();
        
        $product = new Product();
        $product->create($validated);

        return response()->json([
            'message'   => 'Created'
        ], 201);
    }

    /**
     * Show the detail page for resource.
     * 
     * @param Product $product
     * @return View
     */
    public function show(Product $product): View
    {
        return view('products.show', ['product' => $product]);
    }
}