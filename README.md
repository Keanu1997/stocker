# Stocker
This little application is written with the next tools/local environment:
- windows10
- Ubuntu 20.04
- [WSL2](https://docs.microsoft.com/en-us/windows/wsl/install-win10)
- Visual studio with [remote WSL extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-wsl)
- [Docker desktop for windows](https://www.docker.com/products/docker-desktop)


## Create a virtual host
Add the following line to C:\Windows\System32\drivers\etc\hosts:
```
stocker.test    127.0.0.1
```

## Docker quickstart
First build the php-cli and php-fpm image with: `docker compose build --no-cache`

Then fire up your containers with: `docker compose up -d`

## Composer install
Because I chose to keep my composer locally and not in docker we need to run compose install with the `--ignore-platform-reqs` flag. The compose install should be quicker locally.
`composer install --ignore-platform-reqs`

>Be sure to fire the composer install command in the `./src` folder.

## Set folder permissions
For now we have to set the folder permissions of the application manualy. This will be fixed in the future.

First open a bash in you php-cli container with `docker exec -it php-cli bash`.

Then set the permissions `chown -R www-data:www-data /usr/var/nginx/html/storage`